package com.assessment.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.assessment.dao.LoginDao;
import com.assessment.dao.LoginSession;
import com.assessment.dao.impl.LoginDaoImpl;
import com.assessment.service.LoginService;
import com.assessments.models.User;

public class LoginServiceImpl implements LoginService{

	
	
	@Override
	public User Login(HttpServletRequest req, HttpServletResponse resp) {
		User user=new User();
		String email=req.getParameter("email");
		String password=req.getParameter("password");
		
		user.setEmail(email);
		user.setPassword(password);
		 HttpSession session=req.getSession();  
         session.setAttribute("name",user.getName());  
		return user;
		
	}

	

}

package com.assessment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.assessment.dao.LoginSession;

public class LoginSessionImpl implements LoginSession {

	
	public String LoginSession1(String email) {
		String name=null;
		try {
			Connection connection=Connectionprovider.getConnection();
			String query="select name from users where email=?";
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1,email);
			ResultSet resultSet=preparedStatement.executeQuery();
			resultSet.next();
			name=resultSet.getString(1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return name;
	}

}
